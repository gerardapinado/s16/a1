// 3
let num = prompt("Enter Number")
// 4
console.log("The Number you provided: "+num)

for(num; num>0; num--){
	// 5
	if(num<=50){
		console.log("The current value is at 50. Terminating the loop.")
		break;	
	}
	// 6
	if(num%10===0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}
	// 7
	if(num%5===0){
		console.log(num)
	}
}
// 8
let str = "supercalifragilisticexpialidocious"
// 9
let consonants = ""
// 10
for(let x=0; x<str.length; x++){
	// 11
	// if(str[x] == 'a'){ continue;
	// }else if(str[x] == 'e'){ continue;
	// }else if(str[x] == 'i'){ continue;
	// }else if(str[x] == 'o'){ continue;
	// }else if(str[x] == 'u'){ continue;
	// }
	// else{
	// 	//12
	// 	consonants += str[x];
	// }

	if(str[x]=='a' || 
		str[x]=='e' ||
		str[x]=='i' ||
		str[x]=='o' ||
		str[x]=='u')
	{
		continue;
	}else{
		consonants += str[x];
	}
}

console.log(str)
console.log(consonants)